import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditorContainerComponent } from './editor-container/editor-container.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'editor',
    pathMatch: 'full',
  },
  {
    path: 'editor',
    component: EditorContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
