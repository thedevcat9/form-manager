import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { NgFormsManager } from '@ngneat/forms-manager';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppForms } from '../core/models/forms/forms.model';

@Component({
  selector: 'app-details-editor',
  templateUrl: './details-editor.component.html',
  styleUrls: ['./details-editor.component.scss'],
})
export class DetailsEditorComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formsManager: NgFormsManager<AppForms>,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForm();

    this.formsManager
      .valueChanges('details-editor', 'disabledFin')
      .pipe(
        tap((disabled) => {
          if (!disabled) {
            this.form.get('fin').enable();
          } else {
            this.form.get('fin').disable();
          }
        })
      )
      .subscribe();
  }

  getFormError(controlName: string) {
    return this.formsManager.getControl('details-editor', controlName).errors;
  }

  initForm() {
    this.form = this.fb.group({
      fin: ['', [Validators.required], [this.isFINValidd.bind(this)]],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      date: ['', [Validators.required]],
      disabledFin: [false, [Validators.required]],
    });

    this.formsManager.upsert('details-editor', this.form, {
      persistState: true,
      withInitialValue: true,
    });

    this.form.markAllAsTouched();
    this.form.updateValueAndValidity();
  }

  isFINValidd(control: FormControl): Observable<ValidationErrors | null> {
    if (control.value.indexOf('x') !== -1) {
      return of({ invalidFin: true });
    }
    return of(null);
  }
}
