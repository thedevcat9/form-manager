import { Component, OnInit } from '@angular/core';
import { NgFormsManager } from '@ngneat/forms-manager';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { AppForms } from '../core/models/forms/forms.model';

@Component({
  selector: 'app-details-view',
  templateUrl: './details-view.component.html',
  styleUrls: ['./details-view.component.scss'],
})
export class DetailsViewComponent implements OnInit {
  initialValueChanged$: Observable<any>;
  finErrors$: Observable<any>;
  finDisabled$: Observable<any>;
  emailFormErrors$: Observable<any>;
  formValue$: Observable<any>;
  formValidity$: Observable<any>;

  constructor(private formsManager: NgFormsManager<AppForms>) {}

  ngOnInit(): void {
    this.initialValueChanged$ = this.formsManager.initialValueChanged(
      'details-editor'
    );
    this.finErrors$ = this.formsManager.errorsChanges('details-editor', 'fin');

    this.emailFormErrors$ = this.formsManager
      .controlChanges('details-editor', 'email')
      .pipe(pluck('errors'));

    this.formValue$ = this.formsManager.valueChanges('details-editor');

    this.formValidity$ = this.formsManager.validityChanges('details-editor');

    this.finDisabled$ = this.formsManager.disableChanges(
      'details-editor',
      'fin'
    );
  }
}
