import { Component, OnInit } from '@angular/core';
import { NgFormsManager } from '@ngneat/forms-manager';
import { AppForms } from '../core/models/forms/forms.model';

@Component({
  selector: 'app-editor-container',
  templateUrl: './editor-container.component.html',
  styleUrls: ['./editor-container.component.scss'],
})
export class EditorContainerComponent implements OnInit {
  constructor(private formsManager: NgFormsManager<AppForms>) {}

  ngOnInit(): void {}

  clearForm() {
    this.formsManager.setValue(
      'details-editor',
      this.formsManager.getInitialValue('details-editor')
    );
    this.formsManager.clear('details-editor');
  }

  submitForm() {}
}
