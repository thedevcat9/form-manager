export interface AppForms {
  'details-editor': {
    fin: string;
    name: string;
    email: string;
    phone: number;
    date: number;
  };
}
